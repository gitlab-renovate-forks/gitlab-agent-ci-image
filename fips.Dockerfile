# Dockerfile for FIPS builds

# ---------------------------------------------------------------------
# Copied from https://gitlab.com/gitlab-org/build/CNG/-/blob/1c36c9eb6815b29a2aa0fc4494563ac56690ee7c/gitlab-go/Dockerfile.build.fips#L14-14
# and modified to use Go 1.19.x
# ---------------------------------------------------------------------

# UBI version from https://gitlab.com/gitlab-org/build/CNG/-/blob/master/ci_files/variables.yml
ARG UBI_IMAGE=registry.access.redhat.com/ubi9/ubi:9.5

FROM ${UBI_IMAGE} AS builder

ARG TARGETARCH
ARG GO_VERSION=1.23.6
ARG GO_FIPS_TAG=go${GO_VERSION}-1-openssl-fips
ARG GOLANG_DOWNLOAD_URL="https://golang.org/dl/go${GO_VERSION}.linux-${TARGETARCH}.tar.gz"

RUN dnf update -y && \
    dnf install -y --setopt=tsflags=nodocs openssl-devel glibc-devel gcc git

RUN mkdir -p /tmp/go-sdk && \
    curl -fsSL "${GOLANG_DOWNLOAD_URL}" -o golang.tar.gz && \
    tar -C /tmp/go-sdk -xzf golang.tar.gz

RUN git clone \
    https://github.com/golang-fips/go.git \
    --branch "${GO_FIPS_TAG}" \
    --single-branch \
    --depth 1 \
    /tmp/go-fips && \
    git config --global user.email "configure@gitlab.com" && \
    git config --global user.name "CI script" && \
    cd /tmp/go-fips && \
    PATH="${PATH}:/tmp/go-sdk/go/bin" ./scripts/full-initialize-repo.sh && \
    cd ./go/src && \
    PATH="${PATH}:/tmp/go-sdk/go/bin" GOROOT_FINAL=/usr/local/go CGO_ENABLED=1 GOEXPERIMENT=boringcrypto ./make.bash && \
    rm -rf \
        /tmp/go-fips/go/pkg/*/cmd \
        /tmp/go-fips/go/pkg/obj \
        /tmp/go-fips/go/pkg/tool/*/api \
        /tmp/go-fips/go/.git*

# ---------------------------------------------------------------------

FROM ${UBI_IMAGE} AS ci_image

ARG TARGETARCH

RUN dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo && \
    dnf update -y && \
    dnf install -by --nodocs openssl-devel gcc git make docker-ce docker-ce-cli containerd.io && \
    dnf clean all -y

ARG COSIGN_VERSION=2.4.1
ARG CRANE_VERSION=0.20.2

WORKDIR /tmp
RUN curl --fail-with-body -LO https://github.com/sigstore/cosign/releases/download/v${COSIGN_VERSION}/cosign-linux-${TARGETARCH} && \
    chmod +x cosign-linux-${TARGETARCH} && \
    mv cosign-linux-${TARGETARCH} /usr/local/bin/cosign

RUN if [ "${TARGETARCH}" = "amd64" ]; then arch=x86_64; else arch=${TARGETARCH}; fi && \
    echo "DEBUG arch=${arch}" && \
    curl --fail-with-body -LO https://github.com/google/go-containerregistry/releases/download/v${CRANE_VERSION}/go-containerregistry_Linux_${arch}.tar.gz && \
    tar -zxvf go-containerregistry_Linux_${arch}.tar.gz -C /usr/local/bin/ crane && \
    rm -f /tmp/go-containerregistry_Linux_${arch}.tar.gz

COPY --from=builder /tmp/go-fips/go /usr/local/go

RUN ln -sf /usr/local/go/bin/* /usr/local/bin

RUN useradd --create-home --shell /bin/bash ci

USER ci
WORKDIR /home/ci
