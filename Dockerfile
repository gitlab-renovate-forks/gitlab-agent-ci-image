FROM debian:12-slim

ARG TARGETARCH

RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends ca-certificates curl python3 clang git make patch gnupg lsb-release xz-utils \
  && curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg \
  && echo "deb [arch=$TARGETARCH signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null \
  && apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends docker-ce docker-ce-cli containerd.io \
  && rm -rf "/var/lib/apt/lists/*" \
  && ln -sf /usr/bin/python3 /usr/local/bin/python

ENV CC="/usr/bin/clang"

ARG GO_VERSION=1.23.6
ARG BAZELISK_VERSION=v1.25.0
ARG BUILD_DIR=/tmp/build
ARG GO_URL="https://dl.google.com/go/go${GO_VERSION}.linux-${TARGETARCH}.tar.gz"
ARG COSIGN_VERSION=2.4.1
ARG CRANE_VERSION=0.20.2

WORKDIR /tmp
RUN curl --fail-with-body -LO https://github.com/sigstore/cosign/releases/download/v${COSIGN_VERSION}/cosign_${COSIGN_VERSION}_${TARGETARCH}.deb && \
    dpkg -i *.deb && \
    rm -f /tmp/*.deb

RUN if [ "${TARGETARCH}" = "amd64" ]; then arch=x86_64; else arch=${TARGETARCH}; fi && \
    echo "DEBUG arch=${arch}" && \
    curl --fail-with-body -LO https://github.com/google/go-containerregistry/releases/download/v${CRANE_VERSION}/go-containerregistry_Linux_${arch}.tar.gz && \
    tar -zxvf go-containerregistry_Linux_${arch}.tar.gz -C /usr/local/bin/ crane && \
    rm -f /tmp/go-containerregistry_Linux_${arch}.tar.gz

RUN mkdir ${BUILD_DIR} \
  && cd ${BUILD_DIR} \
  && curl --retry 6 -so golang.tar.gz ${GO_URL} \
  && tar -xf golang.tar.gz \
  && mv ${BUILD_DIR}/go /usr/local/go \
  && rm golang.tar.gz \
  && ln -sf /usr/local/go/bin/go /usr/local/bin/ \
  && rm -rf ${BUILD_DIR}

RUN go install github.com/bazelbuild/bazelisk@${BAZELISK_VERSION} \
    && rm -rf /root/go/pkg \
    && mv /root/go/bin/bazelisk /usr/local/bin/bazel

RUN useradd --create-home --shell /bin/bash ci

USER ci
WORKDIR /home/ci
